
# config
FEDORA_RELEASE	:= 39

-include Make.config

# defaults (override in Make.config)
FEDORA_IA32	?= https://archives.fedoraproject.org/pub/archive/fedora-secondary/releases/30/Everything/i386/os/
#FEDORA_MIRROR	?= https://dl.fedoraproject.org/pub/fedora/linux/development/$(FEDORA_RELEASE)/Everything/
FEDORA_MIRROR	?= https://dl.fedoraproject.org/pub/fedora/linux/releases/$(FEDORA_RELEASE)/Everything/
STREAM9_MIRROR	?= https://mirror.stream.centos.org/9-stream/BaseOS/

CACHE_ARCHS	:= x86_64 aarch64
CACHE_FILES	:= images/pxeboot/vmlinuz images/pxeboot/initrd.img images/install.img repodata/repomd.xml

########################################################################

PACKAGES	:= python3-virt-firmware
PACKAGES	+= virt-install
PACKAGES	+= libvirt
PACKAGES	+= libvirt-daemon-qemu
PACKAGES	+= guestfs-tools
PACKAGES	+= pesign
PACKAGES	+= pykickstart

CHOWN_USER	:= sudo chown $(shell id -u):$(shell id -g)

########################################################################

# fedora
FEDORA_KSX	:= $(wildcard kickstart/fedora*.ksx)
FEDORA_KS	:= $(patsubst %.ksx,%.ks,$(FEDORA_KSX))

FEDORA_FIXUP	:= python3 bin/repo-fixup.py
FEDORA_FIXUP	+= --fedora '$(FEDORA_MIRROR)$$basearch/os/'
FEDORA_FIXUP	+= --updates '$(subst /releases/,/updates/,$(FEDORA_MIRROR))$$basearch/'

FEDORA_DISK	:= $(patsubst kickstart/%.ks,%.qcow2,$(FEDORA_KS))
FEDORA_VARS	:= $(patsubst kickstart/%.ks,%.vars,$(FEDORA_KS))
FEDORA_TARS	:= $(patsubst kickstart/%.ks,%.tar.xz,$(FEDORA_KS))

FEDORA_TEST	:= $(patsubst %.qcow2,test-%.qcow2,$(FEDORA_DISK))
FEDORA_TEST	+= $(patsubst %.qcow2,test-%.vars,$(FEDORA_DISK))

# centos stream 9
STREAM9_KSX	:= $(wildcard kickstart/stream9*.ksx)
STREAM9_KS	:= $(patsubst %.ksx,%.ks,$(STREAM9_KSX))

STREAM9_FIXUP	:= python3 bin/repo-fixup.py
STREAM9_FIXUP	+= --baseos '$(STREAM9_MIRROR)$$basearch/os/'
STREAM9_FIXUP	+= --appstream '$(subst /BaseOS/,/AppStream/,$(STREAM9_MIRROR))$$basearch/os/'

STREAM9_DISK	:= $(patsubst kickstart/%.ks,%.qcow2,$(STREAM9_KS))
STREAM9_VARS	:= $(patsubst kickstart/%.ks,%.vars,$(STREAM9_KS))
STREAM9_TARS	:= $(patsubst kickstart/%.ks,%.tar.xz,$(STREAM9_KS))

STREAM9_TEST	:= $(patsubst %.qcow2,test-%.qcow2,$(STREAM9_DISK))
STREAM9_TEST	+= $(patsubst %.qcow2,test-%.vars,$(STREAM9_DISK))

# rhel 9
RHEL9_KS	:= $(patsubst kickstart/stream%.ksx,kickstart/rhel%.ks,$(STREAM9_KSX))

RHEL9_FIXUP	:= python3 bin/repo-fixup.py
RHEL9_FIXUP	+= --baseos '$(RHEL9_REPO)$$basearch/os/'
RHEL9_FIXUP	+= --appstream '$(subst /BaseOS/,/AppStream/,$(RHEL9_REPO))$$basearch/os/'

RHEL9_DISK	:= $(patsubst kickstart/stream%.ks,rhel%.qcow2,$(STREAM9_KS))
RHEL9_VARS	:= $(patsubst kickstart/stream%.ks,rhel%.vars,$(STREAM9_KS))

########################################################################

default: help

help:
	@echo "targets available:"
	@echo "  dnf           - install packages required"
	@echo "  list          - list possible disk images"
	@echo "  ks            - generate kickstart files"
	@echo ""
	@echo "  <name>.qcow2  - create one disk image"
	@echo "  disks-all     - create all disk images"
	@echo "  disks-fedora  - create all disk images"
	@echo "  disks-stream9 - create all disk images"
	@echo ""
	@echo "  clean         - delete temporary files"
	@echo "  realclean     - delete temporary files and disk images"

list:
	@for image in $(FEDORA_DISK) $(STREAM9_DISK) $(RHEL9_DISK); do echo "$$image"; done

dnf:
	sudo dnf install -y $(PACKAGES)

ks: $(FEDORA_KS) $(STREAM9_KS) $(RHEL9_KS)

kickstart/fedora-%.ks: kickstart/fedora-%.ksx
	ksflatten --config $< | $(FEDORA_FIXUP) > $@

kickstart/stream9-%.ks: kickstart/stream9-%.ksx
	ksflatten --config $< | $(STREAM9_FIXUP) > $@

kickstart/rhel9-%.ks: kickstart/stream9-%.ksx
	ksflatten --config $< | $(RHEL9_FIXUP) > $@

# fetch files (to preload caching http proxy)
cache: $(patsubst %,cache-%,$(CACHE_ARCHS))
cache-%:
	for arch in $*; do \
		for file in $(CACHE_FILES); do \
			link=$(FEDORA_MIRROR)$$arch/os/$$file; \
			echo ""; \
			echo "# $$link"; \
			curl --output /dev/null -# "$$link"; \
		done; \
	done; \
	echo ""

disks-all: disks-fedora disks-stream9

disks-fedora: $(FEDORA_DISK)

disks-stream9: $(STREAM9_DISK)

clean:
	rm -f *~ */*~
	rm -f secureboot.rhtest.*
	rm -f secureboot.centos.*
	rm -f $(FEDORA_KS)
	rm -f $(STREAM9_KS)
	rm -f $(RHEL9_KS)

realclean: clean testclean
	rm -f $(FEDORA_DISK) $(STREAM9_DISK) $(RHEL9_DISK)
	rm -f $(FEDORA_VARS) $(STREAM9_VARS) $(RHEL9_VARS)
	rm -f $(FEDORA_TARS) $(STREAM9_TARS)

fedora-%-ia32.qcow2: kickstart/fedora-%-ia32.ks fedora-%-ia32.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(FEDORA_IA32) $<

fedora-%-x64.qcow2: kickstart/fedora-%-x64.ks fedora-%-x64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(FEDORA_MIRROR)x86_64/os/ $<

fedora-%-aa64.qcow2: kickstart/fedora-%-aa64.ks fedora-%-aa64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(FEDORA_MIRROR)aarch64/os/ $<

stream9-%-x64.qcow2: kickstart/stream9-%-x64.ks stream9-%-x64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(STREAM9_MIRROR)x86_64/os/ $<

stream9-%-aa64.qcow2: kickstart/stream9-%-aa64.ks stream9-%-aa64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(STREAM9_MIRROR)aarch64/os/ $<

rhel9-%-x64.qcow2: kickstart/rhel9-%-x64.ks rhel9-%-x64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(RHEL9_REPO)x86_64/os/ $<

rhel9-%-aa64.qcow2: kickstart/rhel9-%-aa64.ks rhel9-%-aa64.vars
	rm -f $@
	bin/run-kickstart-install.sh $@ $(RHEL9_REPO)aarch64/os/ $<

########################################################################
# varstore targets

secureboot.rhtest.ca.pem:
	certutil -L -d /etc/pki/pesign-rh-test -n "Red Hat Test CA" -a \
		| openssl x509 -text > $@

secureboot.rhtest.aa64.vars: secureboot.rhtest.ca.pem
	virt-fw-vars \
		--input /usr/share/edk2/aarch64/vars-template-pflash.raw \
		--output $@ \
		--enroll-redhat \
		--distro fedora \
		--distro centos \
		--add-db OvmfEnrollDefaultKeys $< \
		--secure-boot \
		--print --verbose

secureboot.rhtest.x64.vars: secureboot.rhtest.ca.pem
	virt-fw-vars \
		--input /usr/share/edk2/ovmf/OVMF_VARS.fd \
		--output $@ \
		--enroll-redhat \
		--distro fedora \
		--distro centos \
		--add-db OvmfEnrollDefaultKeys $< \
		--secure-boot \
		--print --verbose

secureboot.centos.aa64.vars:
	virt-fw-vars \
		--input /usr/share/edk2/aarch64/vars-template-pflash.raw \
		--output $@ \
		--enroll-redhat \
		--no-microsoft \
		--distro centos \
		--secure-boot \
		--append-boot-filepath /EFI/centos/shimaa64-centos.efi \
		--print --verbose

secureboot.centos.x64.vars:
	virt-fw-vars \
		--input /usr/share/edk2/ovmf/OVMF_VARS.fd \
		--output $@ \
		--enroll-redhat \
		--no-microsoft \
		--distro centos \
		--secure-boot \
		--append-boot-filepath /EFI/centos/shimx64-centos.efi \
		--print --verbose

secureboot.rhel.aa64.vars:
	virt-fw-vars \
		--input /usr/share/edk2/aarch64/vars-template-pflash.raw \
		--output $@ \
		--enroll-redhat \
		--no-microsoft \
		--distro rhel \
		--secure-boot \
		--append-boot-filepath /EFI/redhat/shimaa64-redhat.efi \
		--print --verbose

secureboot.rhel.x64.vars:
	virt-fw-vars \
		--input /usr/share/edk2/ovmf/OVMF_VARS.fd \
		--output $@ \
		--enroll-redhat \
		--no-microsoft \
		--distro rhel \
		--secure-boot \
		--append-boot-filepath /EFI/redhat/shimx64-redhat.efi \
		--print --verbose

fedora-classic-x64.vars: /usr/share/edk2/ovmf/OVMF_VARS.secboot.fd
	cp $< $@

fedora-uki-cloud-x64.vars: /usr/share/edk2/ovmf/OVMF_VARS.secboot.fd
	cp $< $@

fedora-uki-direct-x64.vars: /usr/share/edk2/ovmf/OVMF_VARS.secboot.fd
	cp $< $@

fedora-uki-grub2-x64.vars: secureboot.rhtest.x64.vars
	cp $< $@

fedora-uki-sdboot-x64.vars: /usr/share/edk2/ovmf/OVMF_VARS.fd
	cp $< $@

fedora-%-aa64.vars: /usr/share/edk2/aarch64/vars-template-pflash.raw
	cp $< $@

fedora-classic-ia32.vars: /usr/share/edk2/ovmf-ia32/OVMF_VARS.fd
	cp $< $@

stream9-classic-aa64.vars: secureboot.centos.aa64.vars
	cp $< $@

stream9-classic-x64.vars: secureboot.centos.x64.vars
	cp $< $@

stream9-uki-grub2-x64.vars: secureboot.rhtest.x64.vars
	cp $< $@

rhel9-classic-aa64.vars: secureboot.rhel.aa64.vars
	cp $< $@

rhel9-classic-x64.vars: secureboot.rhel.x64.vars
	cp $< $@

.NOTINTERMEDIATE: $(FEDORA_VARS) $(STREAM9_VARS) $(RHEL9_VARS)

########################################################################
# testing

test-all: test-fedora test-stream9

test-fedora: $(FEDORA_TEST)
test-fedora-x64: $(filter %-x64.qcow2, $(FEDORA_TEST))
test-fedora-aa64: $(filter %-aa64.qcow2, $(FEDORA_TEST))
test-fedora-ia32: $(filter %-ia32.qcow2, $(FEDORA_TEST))

test-stream9: $(STREAM9_TEST)

testclean:
	rm -f $(FEDORA_TEST)
	rm -f $(STREAM9_TEST)
	rm -f $(RHEL9_TEST)

test-%.qcow2: %.qcow2 test-%.vars
	rm -f $@
	$(CHOWN_USER) $<
	qemu-img create -f qcow2 -F qcow2 -b $< $@

test-%.vars: %.vars
	cp $< $@

########################################################################
# maintainer targets

UPLOAD_DEST := hagrid.kraxel.org:/public/vhosts/www.kraxel.org/fedora-uki

upload:
	rsync -av --progress *.tar.xz $(UPLOAD_DEST)

tarballs-fedora: $(FEDORA_TARS)
tarballs-fedora-x64: $(filter %-x64.tar.xz, $(FEDORA_TARS))
tarballs-fedora-aa64: $(filter %-aa64.tar.xz, $(FEDORA_TARS))
tarballs-fedora-ia32: $(filter %-ia32.tar.xz, $(FEDORA_TARS))

tarballs-stream9: $(STREAM9_TARS)

%.tar.xz: %.qcow2 %.vars
	$(CHOWN_USER) $^
	virt-sparsify --inplace $<
	tar -c --sparse $^ | xz --verbose > $@
