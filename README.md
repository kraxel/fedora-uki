
Fedora UKI (unified kernel image) testing repo
==============================================

Quickstart
----------

```
git clone https://gitlab.com/kraxel/fedora-uki
cd fedora-uki
make help  # print some usage info
make dnf   # install packages needed
make fedora-uki-grub2.qcow2
bin/boot-image-uefi.sh fedora-uki-grub2.qcow2
```

Download from nearby mirror
---------------------------

Create a `Make.config` file which sets the `MIRROR` variables
accordingly.  My home lab config looks like this:

```
FEDORA_MIRROR := http://spunk.home.kraxel.org/mirror/fedora/rsync/f$(FEDORA_RELEASE)-release/Server/x86_64/os/
STREAM9_MIRROR := http://spunk.home.kraxel.org/stream/9-stream/BaseOS/x86_64/os/
```

Background and references
-------------------------

This builds virtual machine test images with unified kernels,
using anaconda kickstart installs.

 * [Fedora Change Proposal Draft](https://fedoraproject.org/wiki/Changes/Unified_Kernel_Support_Phase_1)
 * [copr repo with test packages](https://copr.fedorainfracloud.org/coprs/kraxel/unified.kernel/)
 * [prebuilt virtual machine images](https://www.kraxel.org/fedora-uki/)


Existing kickstart configs
--------------------------

The root password on all images is `root`.

 * `fedora-classic-dual.ks`: traditional image, boots on both bios and uefi.
 * `fedora-classic-uefi.ks`: traditional image, uefi only.
 * `fedora-uki-grub2.ks`: image with unified kernel and grub2 boot loader.
 * `fedora-uki-sdboot.ks`: image with unified kernel and systemd-boot.
