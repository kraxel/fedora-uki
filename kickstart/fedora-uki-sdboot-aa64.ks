# minimal config
rootpw --plaintext root
firstboot --disable
reboot

# partitioning
ignoredisk --only-use=vda
clearpart --all --initlabel --disklabel=gpt --drives=vda
part /boot/efi --size=500 --fstype=efi
part /boot     --size=500 --fstype=ext4 --label=boot
part /         --size=999 --fstype=ext4 --label=root --grow
bootloader --append="console=ttyAMA0"

# minimal package list
%packages
@core
-dracut-config-rescue
dracut-config-generic

# useful tools
efibootmgr

# minimal kernel, no firmware
kernel-uki-virt
kernel-modules-core
-kernel
-kernel-core
-kernel-modules
-linux-firmware

# sd-boot, no shim + grub2
systemd-boot-unsigned
edk2-ext4
-shim-aa64
-grub2-efi-aa64

%end

%post

# install systemd-boot
/usr/bin/bootctl install
cp -a /usr/share/edk2/drivers /boot/efi/EFI/systemd
echo "timeout 5" > /boot/efi/loader/loader.conf

# setup discoverable partitions
/usr/sbin/sfdisk --part-type /dev/vda 2 BC13C2FF-59E6-4262-A352-B275FD6F7172  # Linux extended boot
/usr/sbin/sfdisk --part-type /dev/vda 3 b921b045-1df0-41c3-af44-4c6f280d3fae  # Linux root (aarch64)

%end
