#!/bin/sh

# args
disk="$1"
repo="$2"
kick="$3"

# config
size="5"

# checks
if test ! -f "$kick"; then
    echo "usage: $0 <disk> <repo> <kickstart>"
    exit 1
fi
if test -f "$disk"; then
    echo "exists: $disk"
    exit 1
fi

vars="${disk%.qcow2}.vars"
extra=""

case "$repo" in
    */x86_64/*)
        arch="x86_64"
        cons="ttyS0"
        code="/usr/share/edk2/ovmf/OVMF_CODE.secboot.fd"
        secure="yes"
        extra="$extra --features smm.state=on"
        case "$(uname -m)" in
            x86_64) extra="$extra --cpu host";;  # kvm
            *)      extra="$extra --cpu Opteron_G1";;
        esac
        ;;
    */i386/*)
        arch="x86_64"
        cons="ttyS0"
        code="/usr/share/edk2/ovmf-ia32/OVMF_CODE.fd"
        secure="no"
        ;;
    */aarch64/*)
        arch="aarch64"
        cons="ttyAMA0"
        code="/usr/share/edk2/aarch64/QEMU_EFI-silent-pflash.raw"
        secure="no"
        ;;
    *)
        echo "can't figure arch ($repo)"
        exit 1
        ;;
esac

if test "$(uname -m)" = "$arch"; then
    type="kvm"
else
    type="qemu"
fi

boot="loader=${code}"
boot="${boot},loader.type=pflash"
boot="${boot},loader.readonly=yes"
boot="${boot},loader.secure=${secure}"
boot="${boot},nvram=${vars}"

# go!
bin/xterm-title "install: $disk"
(set -x; virt-install \
     --virt-type "$type" \
     --os-variant rhel9.0 \
     --arch "$arch" \
     --vcpus 2 \
     --memory 4096 \
     --nographics \
     --transient \
     --network user \
     --boot "$boot" \
     $extra \
     \
     --name "install-image-${disk%.qcow2}" \
     --disk "bus=virtio,format=qcow2,sparse=yes,size=${size},path=${disk}" \
     --initrd-inject "${kick}" \
     --extra-args "console=${cons} inst.ks=file:/${kick##*/}" \
     --location "$repo")
