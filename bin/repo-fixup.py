#!/usr/bin/python3
import sys
import optparse

def main():
    parser = optparse.OptionParser()

    # fedora
    parser.add_option('--fedora', dest = 'fedora', type = str,
                      help = 'fedora release repo URL')
    parser.add_option('--updates', dest = 'updates', type = str,
                      help = 'fedora updates repo URL')

    # centos
    parser.add_option('--baseos', dest = 'baseos', type = str,
                      help = 'centos baseos repo URL')
    parser.add_option('--appstream', dest = 'appstream', type = str,
                      help = 'centos appstream repo URL')

    (options, args) = parser.parse_args()

    while True:
        line = sys.stdin.readline()
        if line == '':
            break
        if options.fedora and line.startswith('url'):
            line = f'url --url={options.fedora}'
        if options.baseos and line.startswith('url'):
            line = f'url --url={options.baseos}'
        if line.startswith('repo'):
            if options.fedora and 'name="fedora"' in line:
                line = f'repo --name="fedora" --baseurl={options.fedora}'
            if options.updates and 'name="updates"' in line:
                line = f'repo --name="updates" --baseurl={options.updates}'
            if options.baseos and 'name="baseos"' in line:
                line = f'repo --name="baseos" --baseurl={options.baseos}'
            if options.appstream and 'name="appstream"' in line:
                line = f'repo --name="appstream" --baseurl={options.appstream}'
        print(line.strip())

if __name__ == '__main__':
    sys.exit(main())
