#!/bin/sh
for image in test-*-x64.qcow2; do
    test -f "$image" || continue
    bin/boot-image-uefi.sh $image
done
for image in \
    test-*classic*-x64.qcow2 \
    test-*grub2*-x64.qcow2 \
; do
    test -f "$image" || continue
    bin/boot-image-bios.sh $image
done
for image in test-*-ia32.qcow2; do
    test -f "$image" || continue
    bin/boot-image-ia32.sh $image
done
for image in test-*-aa64.qcow2; do
    test -f "$image" || continue
    bin/boot-image-aa64.sh $image
done
