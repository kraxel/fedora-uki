#!/bin/sh

# args
disk="$1"

# checks
if test ! -f "$disk"; then
    echo "usage: $0 <disk>"
    exit 1
fi

vars="${disk%.qcow2}.vars"
boot="loader=/usr/share/edk2/ovmf-ia32/OVMF_CODE.fd"
boot="${boot},loader.type=pflash"
boot="${boot},loader.readonly=yes"
boot="${boot},nvram=${vars}"

# go!
bin/xterm-title "boot: $disk [uefi,ia32]"
(set -x; virt-install \
     --virt-type kvm \
     --os-variant rhel9.0 \
     --arch x86_64 \
     --memory 4096 \
     --nographics \
     --transient \
     --network user \
     --boot "$boot" \
     \
     --name "boot-image-uefi-${disk%.qcow2}" \
     --disk "bus=virtio,format=qcow2,path=${disk}" \
     --import)
