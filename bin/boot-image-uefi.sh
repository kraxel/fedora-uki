#!/bin/sh

# args
disk="$1"

# checks
if test ! -f "$disk"; then
    echo "usage: $0 <disk>"
    exit 1
fi

code="${EDK2_CODE-/usr/share/edk2/ovmf/OVMF_CODE.secboot.fd}"
vars="${disk%.qcow2}.vars"

boot="loader=${code}"
boot="${boot},loader.type=pflash"
boot="${boot},loader.readonly=yes"
boot="${boot},loader.secure=yes"
boot="${boot},nvram=${vars}"

# go!
bin/xterm-title "boot: $disk [uefi,x64]"
(set -x; virt-install \
     --virt-type kvm \
     --os-variant rhel9.0 \
     --arch x86_64 \
     --vcpus 2 \
     --memory 4096 \
     --nographics \
     --transient \
     --network user \
     --boot "$boot" \
     --features smm.state=on \
     \
     --name "boot-image-uefi-${disk%.qcow2}" \
     --disk "bus=virtio,format=qcow2,path=${disk}" \
     --import)
