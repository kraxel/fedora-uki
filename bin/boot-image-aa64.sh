#!/bin/sh

# args
disk="$1"

# checks
if test ! -f "$disk"; then
    echo "usage: $0 <disk>"
    exit 1
fi

code="${EDK2_CODE-/usr/share/edk2/aarch64/QEMU_EFI-silent-pflash.raw}"
vars="${disk%.qcow2}.vars"

boot="loader=${code}"
boot="${boot},loader.type=pflash"
boot="${boot},loader.readonly=yes"
boot="${boot},nvram=${vars}"

if test "$(uname -m)" == "aarch64"; then
    type="kvm"
else
    type="qemu"
fi

# go!
bin/xterm-title "boot: $disk [uefi,aa64]"
(set -x; virt-install \
     --virt-type "$type" \
     --os-variant rhel9.0 \
     --arch aarch64 \
     --vcpus 2 \
     --memory 4096 \
     --nographics \
     --transient \
     --network user \
     --boot "$boot" \
     \
     --name "boot-image-${disk}" \
     --disk "bus=virtio,format=qcow2,path=${disk}" \
     --import)
