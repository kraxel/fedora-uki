#!/bin/sh

# args
disk="$1"

# checks
if test ! -f "$disk"; then
    echo "usage: $0 <disk>"
    exit 1
fi

# go!
bin/xterm-title "boot: $disk [bios]"
(set -x; virt-install \
     --virt-type kvm \
     --os-variant rhel9.0 \
     --arch x86_64 \
     --vcpus 2 \
     --memory 4096 \
     --nographics \
     --transient \
     --network user \
     --boot bootmenu.enable=on,bios.useserial=on \
     \
     --name "boot-image-bios-${disk%.qcow2}" \
     --disk "bus=virtio,format=qcow2,path=${disk}" \
     --import)
