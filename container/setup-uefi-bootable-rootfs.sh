#!/bin/sh
#
# tweak root filesystem to be EFI-bootable (using virtiofs)
#

# setup /EFI
if test -d /boot/efi/EFI; then
    mv /boot/efi/EFI /EFI
else
    mkdir -p /EFI /boot/efi
fi
ln -s /EFI /boot/efi/EFI

# we link kernels here
mkdir -p /EFI/Linux

# turn off kernel-install plugins which populate /boot
mkdir -p /etc/kernel/install.d
for item in 20-grub 50-dracut 90-loaderentry 90-uki-copy; do
    touch /etc/kernel/install.d/${item}.install
done

# add our own kernel-install plugin
ln -s /usr/local/bin/kernel-install-container.sh \
   /etc/kernel/install.d/99-container.install

# configure cmdline
echo "console=ttyS0 root=virtiofs:root" > /etc/kernel/cmdline
