#!/bin/sh
#
# boot container in qemu
#

# args
image="$1"

# config
mem="4G"
tag="root"

# fedora rpms
#code="/usr/share/edk2/ovmf/OVMF_CODE.fd"
#vars="/usr/share/edk2/ovmf/OVMF_VARS.fd"

# local build
code="/home/kraxel/projects/edk2/Artifacts/ovmf-x64/OVMF_CODE.fd"
vars="/home/kraxel/projects/edk2/Artifacts/ovmf-x64/OVMF_VARS.fd"

set -ex
container=$(podman container create "$image")
mount=$(podman mount "$container")

socket="/tmp/vhostqemu-$$"
trap "rm -f $socket" EXIT

# rust virtiofsd
/usr/libexec/virtiofsd \
    --socket-path="$socket" \
    --shared-dir="$mount" &

qemu-system-x86_64 \
    -nodefaults -display none \
    -enable-kvm \
    -M q35 \
    -m $mem -object memory-backend-memfd,id=mem,size=${mem} -numa node,memdev=mem \
    \
    -drive if=pflash,format=raw,file=${code},readonly=on \
    -drive if=pflash,format=raw,file=${vars},snapshot=on \
    \
    -chardev socket,id=fs,path=$socket \
    -device vhost-user-fs-pci,queue-size=1024,bootindex=1,chardev=fs,tag=${tag} \
    \
    -chardev stdio,id=console \
    -device isa-serial,chardev=console \
