
uefi bootable container experiment
----------------------------------

goal: boot container using the virtiofs support in OVMF instead of
using qemu direct kernel boot.

current state:
* building container works.
* booting the uki (via shim) works.
* mounting rootfs fails
  - b/c uki-virt initrd has no virtiofs support.
