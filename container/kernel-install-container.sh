#!/bin/sh
#
# (1) link (and unlink) kernels to /EFI/Linux
#     - shim accepts only "/EFI/$something" paths
# (2) update BOOTX64.CSV
#     - allows to boot without grub.efi
#     - needed because grub.efi can't read virtiofs
#

# args
COMMAND="${1:?}"
KERNEL_VERSION="${2:?}"
ENTRY_DIR_ABS="$3"
KERNEL_IMAGE="$4"

# config
cmdline="$(cat /etc/kernel/cmdline)"

# figure efi arch name
case "$(uname -m)" in
    aarch64)
        arch="aa64"
        ARCH="AA64"
        ;;
    x86_64)
        arch="x64"
        ARCH="X64"
        ;;
esac

function update_csv() {
    shim="$(ls /EFI/*/shim${arch}.efi)"
    csv="${shim%/*}/BOOT${ARCH}.CSV"
    echo -ne '\xff\xfe' > "$csv"

    ukis="$(ls --sort=time /EFI/Linux/*.efi)"
    for uki in $ukis; do
        name=$(basename $uki .efi)
        echo "shimx64.efi,$name,$uki $cmdline ,comment"
    done \
        | tr '/' '\\' \
        | iconv -f utf-8 -t ucs-2le >> "$csv"
}

case "$COMMAND" in
    add)
        ln ${KERNEL_IMAGE} /EFI/Linux/${KERNEL_VERSION}.efi
        update_csv
        ;;
    remove)
        rm /EFI/Linux/${KERNEL_VERSION}.efi
        update_csv
        ;;
esac
